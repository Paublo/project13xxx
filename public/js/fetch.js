
$(document).ready(function () {
let show = function(btn) {
        
        btn.disabled = true;
        
        btn.innerHTML = "<div class=\"lds-ring\"><div></div><div></div><div></div><div></div></div>";
    };

    let hide = function(btn) {
        btn.innerHTML = "SEND";
        btn.disabled = false;
    };

    
    fetchForm("myform", "myform-sumbit");

    
    function fetchForm(formID, buttonID) {
        let selectForm = document.getElementById(formID); 
        let selectButton = document.getElementById(buttonID);
        let formAction = selectForm.getAttribute("action"); 
        let formMethod = selectForm.getAttribute("method");
        let formInputs = selectForm.querySelectorAll("input");
        let formTextAreas = selectForm.querySelectorAll("textarea");

        
        selectForm.onsubmit = async (e) => {
            e.preventDefault();
            show(selectButton);
            var formData = new FormData();

            for (var i = 0; i < formInputs.length; i++) {
                formData.append(formInputs[i].name, formInputs[i].value);
            }
            for (var i = 0; i < formTextAreas.length; i++) {
                formData.append(formTextAreas[i].name, formTextAreas[i].value);
            }
            let response = await fetch(formAction, {
                method: formMethod,
                headers: {
                    "Accept": "application/json"
                },
                body: formData
            });
            if (response.ok) {
                let result = await response.json();
                localStorage.clear();
                if (!$("#modal").hasClass("dn")) history.back();
                hide(selectButton);
                alert(result.title + "\n" + result.message);
            } else {
                alert("Ошибка!\nПожалуйста, повторите попытку!");
                hide(selectButton);
            }
        };
    }
});