$(document).ready(function () {



    //Slider Slick
     $(".gallery").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        dots: true,
        arrows: true,
        adaptiveHeight: true,
        lazyLoad: "ondemand",
        responsive: [
            {
                breakpoint: 650,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    arrows: true,
                    dots: false
                }
          }]
    });

    
    $(".certificate-slider").slick({
        slidesToShow: 3,
        infinite: true,
        prevArrow: "<i class=\"material-icons prev2\">keyboard_arrow_left</i>",
        nextArrow: "<i class=\"material-icons next2\">keyboard_arrow_right</i>",
        arrows: false,
        dots: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    arrows: true,
                    slidesToShow: 2
                }
      }]
    });



    $("#nav-btn-open").click(function () {
       
        $("body").toggleClass("openNav");
        $(".second_menu").animate({
            width: "70%"
        });
    });
    
    $("#nav-btn-cancel").click(function () {
        console.log("Menu closed");
        $(".second_menu").animate({
            width: "0%"
        }, {
         
            complete: function () {
                $("body").toggleClass("openNav");
            }
        });
    });

//Modal Animation
    var showModal = function () {
        $("#modal").toggleClass("dn");
        let modal = document.getElementById("modal");
        animateModal({
            duration: 200,
            
            timing: function (timeFraction) {
                return timeFraction;
            },
            
            draw: function (progress) {
                modal.style.opacity = progress;
                if ($(document).width() > 650) {
                    modal.style.marginTop = progress * 4 + "rem";
                }
            }
        });
    }

    function animateModal({duration,timing,draw }) {
        let start = performance.now();
        requestAnimationFrame(function animate(time) {
            let timeFraction = (time - start) / duration;
            if (timeFraction > 1) timeFraction = 1;
            let progress = timing(timeFraction);
            draw(progress);
            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }
        });
    }
   
    window.addEventListener("popstate", function (e) {
        showModal();
    });
    
    let buttons = document.querySelectorAll(".slbutt");
    
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function (e) {
            if (e.target.tagName !== "A") return; 
            var state = {
                page: e.target.getAttribute("href")
            };
            
            history.pushState(state, "", state.page);
            showModal();
            e.preventDefault();
        });
    }

});
