import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { createStore } from 'react-redux';


class Form extends React.Component {

    constructor(props) {
        super(props);
        var name = localStorage.getItem("name") || "";
        var nameIsValid = this.validateName(name);
        var mail = localStorage.getItem("mail") || "";
        var comment =  localStorage.getItem("comment") || "";

        
        this.state = {
            name: name,
            mail: mail,
            comment: comment,
            nameValid: nameIsValid
        }

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeMail = this.handleChangeMail.bind(this);
        this.handleChangeComment = this.handleChangeComment.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);

    }

   
     show(btn) {
        btn.disabled = true;
        btn.innerHTML = "<div class=\"lds-ring\"><div></div><div></div><div></div><div></div></div>";
    }
    
    hide(btn) {
        btn.innerHTML = "SEND";
        btn.disabled = false;
    }

    
    validateName(name) {
        return name.length > 2;
    }

    
    handleChangeName(event) {
        let val = event.target.value;
        var valid = this.validateName(val);
        this.setState({
            name: event.target.value,
            nameValid: valid
        });
        localStorage.setItem("name", event.target.value);
    }

    
    handleChangeMail(event) {
        
        var val = event.target.value;
        this.setState({
            mail: event.target.value
        });
        localStorage.setItem("mail", event.target.value);
    }

    
    handleChangeComment(event) {
        var val = event.target.value;
        console.log("Comment: " + val);
        this.setState({
            comment: event.target.value
        });
        localStorage.setItem("comment", event.target.value);
    }

    
    resetForm() {
        localStorage.setItem("name", "");
        localStorage.setItem("mail", "");
        localStorage.setItem("comment", "");
        this.setState({
            name: "",
            mail: "",
            comment: "",
        });
        
        $(".name1").value = "";
        history.back();
    }

    
    handleSubmit(e) {
        
        e.preventDefault();
        
        if (this.state.nameValid === true) {
            this.show(document.getElementById("modal-myform-sumbit"));
            fetch("https://formcarry.com/s/u3r9UuqIzws", {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            }).then(
                (response) => (response.json())
            ).then((response) => {
                
                if (response.status === "success") {
                    
                    alert("Сообщение отправлено!!!!");
                    
                    this.hide(document.getElementById("modal-myform-sumbit"));
                    
                    this.resetForm();
               
                } else if (response.status === "fail") {
                    alert("Ошибка отправки!")
                    this.hide(document.getElementById("modal-myform-sumbit"));
                }
            })
        } else {
            alert("Ошибка ввода!");
        }
    }
    render() {
        return (
            <div className="forma container">
                <div className="form_content">
                    <div className="contacts1">
                        <div className="title">AVIS ELECTRONICS REACT</div>
                        <div>+86 (186) 66660854</div>
                        <div className="email">
                            <p className="label">E-Mail: </p><a href="#">saFFFFles@aviselectronics.com</a>
                        </div>
                        <div>Mon-Fri: 09:00 - 18:00, <br /> Sat: 09:00 - 12:00 (UTC +08:00)</div>
                    </div>
                    <form id="modal-myform" onSubmit={this.handleSubmit} method="POST" accept-charset="UTF-8">
                        <input className="name1 db" name="firstName" placeholder="Name" type="text" value={this.state.name} onChange={this.handleChangeName}/>
                        <input className="name1 db" name="email" placeholder="E-mail" type="email" value={this.state.mail} onChange={this.handleChangeMail}/>
                        <textarea className="name2 db" name="comment" placeholder="Comment" rows="5" value={this.state.comment} onChange={this.handleChangeComment}/>
                        <button id="modal-myform-sumbit" className="btn butt3" type="submit">Send</button>
                    </form>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Form/>, document.getElementById('modal'));